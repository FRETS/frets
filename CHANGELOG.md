# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.2.5"></a>
## [0.2.5](https://gitlab.com/FRETS/frets/compare/v0.2.4...v0.2.5) (2018-04-14)



<a name="0.2.4"></a>
## [0.2.4](https://gitlab.com/FRETS/frets/compare/v0.2.3...v0.2.4) (2018-04-12)



<a name="0.2.3"></a>
## [0.2.3](https://gitlab.com/FRETS/frets/compare/v0.2.1...v0.2.3) (2018-03-27)



<a name="0.2.1"></a>
## [0.2.1](https://gitlab.com/FRETS/frets/compare/v0.1.2...v0.2.1) (2018-03-26)



<a name="0.1.2"></a>
## [0.1.2](https://gitlab.com/FRETS/frets/compare/v0.1.1...v0.1.2) (2017-11-29)



<a name="0.1.1"></a>
## 0.1.1 (2017-11-29)



# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.
